# **R code developed for the statistical analysis of data from the IMOTEP project**.

[![License](https://img.shields.io/badge/License-AGPL_v3-orange.svg)](https://gitlab.ifremer.fr/bioinfo/bioanalysis/public/imotep/-/blob/main/LICENCE?ref_type=heads)
[![Version](https://img.shields.io/badge/version-Final%20version-red?labelColor=000000)](https://gitlab.ifremer.fr/bioinfo/bioanalysis/public/imotep/-/tree/main?ref_type=heads)
[![R](https://img.shields.io/badge/R-4.3.1-23aa62.svg?labelColor=000000)](https://cran.r-project.org/)
[![Authors](https://img.shields.io/badge/Authors-Enora%20Briand%2C%20Charlotte%20Nef%20and%20Cyril%20Noël-blue.svg)]()
[![Publication](https://img.shields.io/badge/Publication-Submitted-yellow.svg)]()

## Introduction

This project contains all the R code developed for the biostatistical analyses of data from the IMOTEP project, including:

- Processing of the raw ASV table, with filtering of false-positive ASVs and contaminants
- Performing statistical tests
- Visualizing results, including generating figures for publication

## How to download the repository

```bash
git clone https://gitlab.ifremer.fr/bioinfo/bioanalysis/public/imotep.git
```

## Repository content

```bash
imotep/
├── assets          # Resources or auxiliary files
├── data            # Raw and processed datasets used for analysis
├── figures         # Generated figures and visualizations
└── stat_results    # Outputs of statistical analyses, including summary tables and results
```

## Credits

This repository is released under the GNU Affero General Public License, Version 3.0 (AGPL).

## Support

For further information or any questions, don't hesitate to get in touch with the authors